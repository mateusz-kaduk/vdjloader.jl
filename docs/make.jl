using VDJLoader
using Documenter

DocMeta.setdocmeta!(VDJLoader, :DocTestSetup, :(using VDJLoader); recursive=true)

makedocs(;
    modules=[VDJLoader],
    authors="Mateusz Kaduk <mateusz.kaduk@gmail.com> and contributors",
    repo="https://gitlab.com/mateusz-kaduk/VDJLoader.jl/blob/{commit}{path}#{line}",
    sitename="VDJLoader.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://mateusz-kaduk.gitlab.io/vdjloader.jl",
        edit_link="main",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
