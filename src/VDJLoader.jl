module VDJLoader
    using CSV
    using OneHotArrays
    const columns = [:v_call, :d_call, :j_call,
                    :v_sequence_start, :v_sequence_end,
                    :d_sequence_start, :d_sequence_end,
                    :j_sequence_start, :j_sequence_end,
                    :sequence
                    ]

    export full_read
    export vdj_region
    export v_region
    export preprocess
    export produce

    """
    Producer that reads TSV in MiAIRR format and batches up rows.
    File is read with chunking so arbitrary size file is possible.
    Missing V, D or J calls are skipped.
    """
    function produce(path::String; n=Threads.nthreads())
        chunks = CSV.Chunks(path, delim='\t', select = columns)
        function producer(c::Channel)
            for chunk in chunks
                for row in chunk
                    if !ismissing(row[:v_call]) & !ismissing(row[:d_call]) & !ismissing(row[:j_call])
                        put!(c, row)
                    end
                end
            end
        end
        return Channel(producer, n, spawn=true)
    end

    """
    Helper function to extract VDJ region
    """
    function vdj_region(row)
        row[:sequence][row[:v_sequence_start]:row[:j_sequence_end]]
    end

    """
    Helper function to extract V region
    """
    function v_region(row)
        row[:sequence][row[:v_sequence_start]:row[:v_sequence_end]]
    end

    """
    Helper function to extract whole read
    """
    function full_read(row)
        row[:sequence]
    end

    """
    Processor helper function which allocates padded batch.
    """
    function processor(result, product::Channel, finished; batch_size=128, region=vdj_region)
        while isopen(product)
            batch = Iterators.take(product, batch_size)
            sequence = [region(r) for r in batch]
            sequence_length = length.(sequence)
            padding = maximum(sequence_length)
            padded_sequence = zeros(Float32, 4, length(sequence), padding)
            v_call = [r[:v_call] for r in batch]
            d_call = [r[:d_call] for r in batch]
            j_call = [r[:j_call] for r in batch]
            for (i,s) in enumerate(sequence)
                z = onehotbatch(s, "ATGC")
                padded_sequence[:, i, 1:size(z, 2)] .= z
            end
            put!(result, (v_call, d_call, j_call, padded_sequence, sequence_length))
        end
        Threads.atomic_add!(finished, 1)
    end

    """
    Preprocessing function which spawns multiple threads and binds them to channel.
    """
    function preprocess(f, product::Channel, batch_size::Int; region=vdj_region);
        n = Threads.nthreads()
        result = Channel{Tuple}(n)
        finished = Threads.Atomic{Int}(0);
        tasks = []
        for i in 1:n
            task = Threads.@spawn processor(result, product, finished, batch_size=batch_size, region=region)
            push!(tasks, task)
        end
        while (finished.value != n) | isready(result)
            batch = take!(result)
            f(batch)
        end
        close(result)
    end
end
