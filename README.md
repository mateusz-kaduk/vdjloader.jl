# VDJLoader

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://mateusz-kaduk.gitlab.io/vdjloader.jl/dev/)
[![Build Status](https://gitlab.com/mateusz-kaduk/VDJLoader.jl/badges/main/pipeline.svg)](https://gitlab.com/mateusz-kaduk/VDJLoader.jl/pipelines)
[![Coverage](https://gitlab.com/mateusz-kaduk/VDJLoader.jl/badges/main/coverage.svg)](https://gitlab.com/mateusz-kaduk/VDJLoader.jl/commits/main)

The VDJLoader reads [MiAIRR rearrangment format](https://docs.airr-community.org/en/stable/datarep/rearrangements.html), typically produced by compilant tools (i.e [IgBLAST](https://ncbi.github.io/igblast/)). It extracts VDJ, V or full read from the TSV file, performs one-hot encoding of the nucleotide sequence and returns padded with zeros batch of encoded sequences, as well as V, D and J calls.
Above operations are performed with multi-threading to reduce overhead related to preprocessing during trianing.

## Installing

From Julia REPL
```
]add https://gitlab.com/mateusz-kaduk/vdjloader.jl
```

## Usage

This replaces training loop, there is no shuffling happening so assumption is sufficiently big data and already randomized
```
batch_size = 256
product = VDJLoader.produce("assigned.tsv.gz")
VDJLoader.preprocess(product, batch_size) do batch
    # Here goes training code
end
```

Optionally different columns can be selected with `vdj_region`, `v_region` and `full_read` by
```
VDJLoader.preprocess(product, batch_size, region=full_read) do batch
    # Here goes training code
end
```
